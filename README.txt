
ADD YOUR BRANCH BELOW AND DESCRIBE ITS PURPOSE
==============================================

ONEALL @ https://dev-develop.pantheonsite.io
--------------------------------------------
Add and enable module OneAll Social Login.
https://www.drupal.org/project/social_login


BENNY @ https://dev-sandbox-sitename.pantheonsite.io
----------------------------------------------------
Add HTTPS with CloudFlare and redirect incoming requests.
Also testing pushing from Kalabox dev environment.


VIDEOFILTER
-----------
Add, enable and configure module Video Filter


DOLEBIT-001
-----------
Add bootstrap sub-theme named “Dolebit”